import unittest

LAT_MAX = 88
LAT_MIN = 83.9  # test
# LAT_MIN = -60.1 # TODO uncomment
LAT_STEP = -2

LON_MIN = -180
LON_MAX = -174.9  # test TODO
# LON_MAX = 180.1 # TODO uncomment
LON_STEP = 2.5

SNT_NIL = "2000/00/00"
SNT_YEAR_NIL = 2000
SNT_MONTH_NIL = 0
SNT_DAY_NIL = 0

KIND_ID_NIL = 0
KIND_ID_CO = 1
KIND_ID_CO2 = 2
KIND_ID_O3 = 3

LOC_ID_NIL = 0

MEA_NIL = 9999

#input date separators allowed
SEPS = [",", ".", "/", "-"]
# output date separator
DSP = "-"

class Measure:

    def __init__(self, snaptime=SNT_NIL, lat=LAT_MAX, lon=LON_MIN, meas=MEA_NIL, kind_id=KIND_ID_NIL, loc_id=LOC_ID_NIL):
        self.year = SNT_YEAR_NIL
        self.month = SNT_MONTH_NIL
        self.day = SNT_DAY_NIL
        self.lat = lat
        self.lon = lon
        self.kind_id = kind_id
        self.meas = meas

        self.loc_id = loc_id
        if not snaptime == SNT_NIL:
            self.parse_snaptime(snaptime)

    def parse_snaptime(self, snaptime):

        for sep in SEPS:
            split = snaptime.split(sep)
            parsed = len(split) == 3
            if parsed:
                break
        if parsed:
            self.year = split[0]
            self.month = split[1]
            self.day = split[2]

    def snaptime(self):
        return str(self.year) + DSP + str(self.month) + DSP + str(self.day)

    def has_id(self):
        return not self.loc_id == LOC_ID_NIL
    
    def index(self):
        return (self.lat, self.lon)


class SimpleTestCase(unittest.TestCase):
    def testA(self):
        snaptime = "2012.12.12"
        m = Measure(snaptime, 30, -180, 4.1234)
        snt = m.snaptime()
        self.assert_( "2012-12-12" == snt )
        # print "tested"

if __name__ == "__main__":
    unittest.main()