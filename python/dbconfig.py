
#dev
DB_CONFIG = {
    'user': 'dsa_user',
    'password': 'S3creta',
    'host': '127.0.0.1',
    'port': '53306',
    'database': 'dsa',
    'raise_on_warnings': True,
}

"""
#pre
DB_CONFIG = {
    'user': 'dsa_user',
    'password': 'S3creta',
    'host': '127.0.0.1',
    'port': '3306',
    'database': 'dsa',
    'raise_on_war': True,
}
"""
"""
#pro
DB_CONFIG = {
    'user': 'dsa_user',
    'password': 'S3creta',
    'host': 'dename-sullust.no-ip.org',
    'port': '5006',
    'database': 'dsa',
    'raise_on_warnings': True,
}

#pro2
DB_CONFIG = {
    'user': 'clmtpp',
    'password': '1$nIh%e77',
    'host': '213.192.239.225',
    'port': '3306',
    'database': 'climateapp',
    'raise_on_warnings': True,
}
"""
