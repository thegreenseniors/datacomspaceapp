from dbconfig import DB_CONFIG
from measure import Measure
import mysql.connector


kin_cnt = "SELECT COUNT(*) AS cnt FROM kinds"
loc_max = "SELECT MAX(loc_id) AS max FROM locations"
loc_add = "INSERT INTO locations(loc_id, lat, lon) VALUES (%s, %s, %s)"
loc_cnt = "SELECT COUNT(*) AS cnt FROM locations"
loc_sel = "SELECT loc_id FROM locations WHERE lat=%s AND lon=%s"
loc_era = "DELETE FROM locations WHERE 0=0"
mea_add = "INSERT INTO measures(snaptime, loc_id, kind, meas) VALUES (%s, %s, %s, %s)"
mea_cnt = "SELECT COUNT(*) AS cnt FROM measures"
mea_era = "DELETE FROM measures WHERE 0=0"

class Database:

    def __init__(self, db_config):
        self.db_config = db_config
        self.cnx = 0
        self.cursor = 0
        self.immediate = 0
        self.max_loc_id = 0
        # self.loc_id_idx = {}


    #startup
    def connect(self):
        print self.db_config['host'], self.db_config['port']
        self.cnx = mysql.connector.connect(**self.db_config)
        self.cursor = self.cnx.cursor()
        self.cursor_sel_loc = self.cnx.cursor()
        self.cursor_ins_loc = self.cnx.cursor()
        self.cursor_ins_mea = self.cnx.cursor()


    def commit(self):
        self.cnx.commit()
        self.immediate = True

    # kind


    def kin_count(self):
        self.cursor.execute(kin_cnt)
        for cnt in self.cursor:
            res = cnt[0]
        return res

    # loc


    def loc_create(self, mea):
        self.max_loc_id += 1
        self.cursor_ins_loc.execute(loc_add, (self.max_loc_id, mea.lat, mea.lon))
        self.cnx.commit() # always
        mea.loc_id = self.max_loc_id
        #self.loc_id_idx[mea.index()] = self.max_loc_id
        return mea.loc_id


    def loc_count(self):
        self.cursor.execute(loc_cnt)
        for cnt in self.cursor:
            res = cnt[0]
        return res


    def loc_max(self):
        self.cursor.execute(loc_max)
        if self.immediate:
            self.cnx.commit()


    def loc_find(self, mea):
        #idx = mea.index()
        #if idx in self.loc_id_idx:
        #    return self.loc_id_idx[idx]
        self.cursor_sel_loc.execute(loc_sel, (mea.lat, mea.lon))
        res = mea.loc_id
        for cnt in self.cursor:
            res = cnt[0]
            mea.loc_id = res
        return res


    def loc_erase(self):
        self.cursor.execute(loc_era)
        if self.immediate:
            self.cnx.commit()

    # mea


    def mea_create(self, mea):
        self.loc_find(mea)
        if not mea.has_id():
            self.loc_create(mea)
        self.cursor_ins_mea.execute(mea_add, (mea.snaptime(), mea.loc_id, mea.kind_id, mea.meas))
        if self.immediate:
            self.cnx.commit()


    def mea_count(self):
        self.cursor.execute(mea_cnt)
        for cnt in self.cursor:
            res = cnt[0]
        return res


    def mea_erase(self):
        self.cursor.execute(mea_era)
        if self.immediate:
            self.cnx.commit()

    # close


    def release(self):
        self.cursor.close()
        self.cnx.close()


def test():
    db = Database(DB_CONFIG)
    db.connect()
    kin_cnt = db.kin_count()
    assert 3 == kin_cnt

    db.loc_erase()  # idempotent
    loc_cnt = db.loc_count()
    assert 0 == loc_cnt

    mea = Measure("2007-06-05", 40, 2.5, 7.124, 1)
    # db.loc_create(mea)
    # loc_cnt =  db.loc_count()
    # assert 1 == loc_cnt

    db.mea_create(mea)
    mea_cnt = db.mea_count()
    assert 1 == mea_cnt
    db.mea_erase()  # idempotent
    mea_cnt = db.loc_count()
    assert 1 == mea_cnt

    db.loc_erase()  # idempotent
    loc_cnt = db.loc_count()
    assert 0 == loc_cnt

    db.release()

if __name__ == "__main__":
    test()
