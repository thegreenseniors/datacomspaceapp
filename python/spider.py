from os import listdir
from datetime import datetime
from dbconfig import DB_CONFIG
import database as database
import parser as parser


URL_OLD = 'ftp://acdisc.gsfc.nasa.gov/ftp/data/s4pa/Aqua_AIRS_Level3/AIRX3C2D.005/'
URL_NEW = 'ftp://acdisc.gsfc.nasa.gov/ftp/data/s4pa/Aqua_AIRS_Level3/AIRS3C2D.005/'
PREFIX = "AIRS."


def retrieve_nasa_info(url):
    # exec wget -r url
    pass


def hdf_to_csv():
    # exec R.source('../R/CO2_hdf5_to_csv.R')
    pass


def read_dir(folder_path):
    # res.append("AIRS.2012.12.12.L3.CO2file") # e.g.
    res = listdir(folder_path)
    res[:] = [value for value in res if value.endswith(parser.EXTENSION)]
    res.sort()
    return res

"""

def world_iterate(db, iter_item):
    las = np.arange(LAT_MAX, LAT_MIN, LAT_STEP)
    los = np.arange(LON_MIN, LON_MAX, LON_STEP)

    lat = 89.5
    for lon in los:
        iter_item( db, lat, lon)

    for lat in las:
        for lon in los:
            iter_item(db, lat, lon)


def iter_insert_measures(db, lat, lon):
    loc_id = db.loc_find(db, lat, lon)
    mea = Measure(snaptime, )
    db.mea_create(mea)
"""


def csv_input_folder_iterate(folder_path, output_file=None, database=None):
    file_names = read_dir(folder_path)
    for file_name in file_names:
        print print_time() + " " + file_name
        meas = parser.read_csv(folder_path, file_name, PREFIX)
        if not output_file is None:
            parser.dump_csv(meas, output_file)
        if not database is None:
            database.immediate = False
            for mea in meas:
                database.mea_create(mea)
            database.commit()

def print_time():
    return str(datetime.now())

def test():
    retrieve_nasa_info(URL_NEW)
    hdf_to_csv()

    # database fulfill
    db = database.Database(DB_CONFIG)

    db.connect()
    db.loc_erase() # idempotent
    db.mea_erase() # idempotent
    print db.mea_count()

    print print_time()

    #Loop
    # csv_input_folder_iterate("input/", "test/output.csv")
    # csv_input_folder_iterate("../../../csv/", "merged.csv", db)
    #csv_input_folder_iterate("../../../csv1501/", "../../../merged1501.csv", db)
    
    #csv_input_folder_iterate("../../../csv1501_3/", None, db)
    #csv_input_folder_iterate("../../../csv1501_6/", None, db)
    #csv_input_folder_iterate("../../../csv1501/", None, db)
    
    csv_input_folder_iterate("../../../csv/", None, db)

    print db.mea_count()
    db.release()

    print print_time()

if __name__ == "__main__":
    test()