from scipy.stats import norm

RANDOM_FILENAME = 'random.csv'
RANDOM_MEASURES = 8000
RANDOM_MEAN = 5
RANDOM_DEV = 1


def mock_build(file_name, size, mean, dev):
    rv = norm.rvs(size=size, loc=mean, scale=dev)
    f=open(file_name, 'w+')
    for v in rv:
        f.write(str(v))
        f.write(',')
    f.close()


# RANDOM_MEASURES following normal distribution
if __name__ == "__main__":
    mock_build(RANDOM_FILENAME, RANDOM_MEASURES, RANDOM_DEV)
