import os
import csv
import sys
import measure as measure


EXTENSION = ".csv"

HEADER = "date(yyyy-mm-dd),lon,lat,CO2"

# date format: yyyy.mm.dd
DATE_LEN = 10
# date escape
DFM = "\""
# column separator
SEP = ","
# new line
NLN = "\n"


def read_csv(folder_path, file_name, PREFIX):
    assert file_name.startswith(PREFIX)
    assert file_name.endswith(EXTENSION)
    # snaptime = "2012.12.12"
    snaptime = file_name[len(PREFIX):len(PREFIX)+DATE_LEN]

    res = []
    try:
        with open(folder_path + file_name, "rb") as f:
            reader = csv.reader(f)
            lines = list(reader)
            lines = lines[1:] # avoid header. assert?
    
            ln = 0
            for line in lines:
                ln += 1
                try:
                    lat = float(line[2])
                    lon = float(line[1])
                    val = float(line[3])
                except:
                    # print fileName + ":" + str(ln) + ":" + str(sys.exc_info()[0])
                    continue
                m = measure.Measure(snaptime, lat, lon, val, measure.KIND_ID_CO2)
                res.append(m)
    except:
        print "fail at:" + file_name + ":" + str(sys.exc_info()[0])
    return res


def dump_csv(meas, file_path):
    created = os.path.isfile(file_path) and os.path.getsize(file_path) > 0
    f = open(file_path, "a")
    if created:
        f.write(HEADER)
    for g in meas:
        f.write(DFM+g.snaptime()+DFM+SEP+str(g.lon)+SEP+str(g.lat)+SEP+str(g.meas)+NLN)
    f.close()
