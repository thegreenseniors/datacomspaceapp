CREATE TABLE kinds (
    kind TINYINT PRIMARY KEY,
    label varchar(20),
    description varchar(80),
    copyright varchar(255)
)
ENGINE = MYISAM
;

CREATE TABLE locations (
    loc_id SMALLINT, -- PRIMARY KEY,
    lat FLOAT NOT NULL,
    lon FLOAT NOT NULL,
	INDEX coor_idx (lat, lon)
)
ENGINE = MYISAM
;

CREATE TABLE measures (
    snaptime DATE NOT NULL,
    loc_id SMALLINT NOT NULL,
    kind TINYINT NOT NULL,
    meas FLOAT
)
ENGINE = MYISAM
;

