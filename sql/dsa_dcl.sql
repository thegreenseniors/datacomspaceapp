CREATE DATABASE dsa
;

CREATE USER dsa_user identified by 'secret'
;

GRANT ALL PRIVILEGES ON dsa.* TO 'dsa_user'@'%' IDENTIFIED BY 'secret'
;