
SELECT COUNT(*) AS n_kinds 
FROM kinds
;


SELECT 
    snaptime, lat, lon, meas
FROM
    kinds kin,
    locations loc,
    measures mea
WHERE
    mea.kind = kin.kind 
AND mea.loc_id = loc.loc_id
-- AND snaptime= date("2015/01/01")
AND loc.loc_id <= 1
ORDER BY snaptime , lat , lon , meas
;


SELECT COUNT(*) AS n_loc
FROM locations
;

SELECT COUNT(*) AS n_mea
FROM measures
;

SELECT *
FROM locations 
WHERE loc_id < 10
order by lat, lon
;

/* 1, 88, -145
*/

SELECT snaptime, loc_id -- count(*) as cnt
FROM measures
WHERE 
snaptime= date("2015/01/01")
order by loc_id
;
 

